# chartjs-adapter-dayjs

## Note

This adapter was made to override existing implementations for Chart.js adapters. This has not fully been tested.

## Overview

This adapter allows the use of Dayjs with Chart.js. Dayjs is a very heavy library and thus not recommended for client-side development. However, it was previously the only library supported by Chart.js and so continues to be supported. You may prefer [chartjs-adapter-date-fns](https://github.com/chartjs/chartjs-adapter-date-fns) for a minimal bundle size or [chartjs-adapter-luxon](https://github.com/chartjs/chartjs-adapter-luxon) for larger bundle size with additional functionality included such as i18n and time zone support.

Requires [Chart.js](https://github.com/chartjs/Chart.js/releases) **3.0.0** or later and [Dayjs](https://day.js.org/) **1.9.7** or later.

**Note:** once loaded, this adapter overrides the default date-adapter provided in Chart.js (as a side-effect).

## Installation

### npm

```
npm install dayjs https://gitlab.com/rickbs/chartjs-adapter-dayjs --save
```

```javascript
import { Chart } from 'chart.js';
import 'chartjs-adapter-dayjs';
```

## Configuration

Read the [Chart.js documention](https://www.chartjs.org/docs/latest) for possible date/time related options. For example, the time scale [`time.*` options](https://www.chartjs.org/docs/latest/axes/cartesian/time.html#configuration-options).

## Development

You first need to install node dependencies (requires [Node.js](https://nodejs.org/)):

```
> npm install
```

## License

`chartjs-adapter-dayjs` is available under the [MIT license](LICENSE.md).
