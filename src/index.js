'use strict';

import Chart from 'chart.js';

import dayjs from 'dayjs';

const advancedFormat = require('dayjs/plugin/advancedFormat');
const quarterOfYear = require('dayjs/plugin/quarterOfYear');
const isoWeek = require('dayjs/plugin/isoWeek');
const localizedFormat = require('dayjs/plugin/localizedFormat');
const customParseFormat = require('dayjs/plugin/customParseFormat');

// Needed to handle quarter format
dayjs.extend(advancedFormat);

// Needed to handle adding/subtracting quarter
dayjs.extend(quarterOfYear);

// Needed to handle localization format
dayjs.extend(localizedFormat);

// Needed to handle the custom parsing
dayjs.extend(customParseFormat);

dayjs.extend(isoWeek);

const FORMATS = {
  datetime: 'MMM D, YYYY, h:mm:ss a',
  millisecond: 'h:mm:ss.SSS a',
  second: 'h:mm:ss a',
  minute: 'h:mm a',
  hour: 'hA',
  day: 'MMM D',
  week: 'll',
  month: 'MMM YYYY',
  quarter: '[Q]Q - YYYY',
  year: 'YYYY'
};

Chart._adapters._date.override({
  // _id: 'dayjs', DEBUG,
  formats: () => FORMATS,
  parse: function(value, format) {
    const valueType = typeof value;

    if (value === null || valueType === 'undefined') {
      return null;
    }

    if (valueType === 'string' && typeof format === 'string') {
      value = dayjs(value, format);
    } else if (!(value instanceof dayjs)) {
      value = dayjs(value);
    }
    return value.isValid() ? value.valueOf() : null;
  },
  format: function(time, format) {
    return dayjs(time).format(format);
  },
  add: function(time, amount, unit) {
    return dayjs(time).add(amount, unit).valueOf();
  },
  diff: function(max, min, unit) {
    return dayjs(max).diff(dayjs(min), unit);
  },
  startOf: function(time, unit, weekday) {
    if (unit === 'isoWeek') {
      // Ensure that weekday has a valid format
      // const formattedWeekday

      const validatedWeekday = typeof weekday === 'number' && weekday > 0 && weekday < 7
        ? weekday
        : 1;

      return dayjs(time).isoWeekday(validatedWeekday).startOf('day').valueOf();
    }

    return dayjs(time).startOf(unit).valueOf();
  },
  endOf: function(time, unit) {
    return dayjs(time).endOf(unit).valueOf();
  }
});
